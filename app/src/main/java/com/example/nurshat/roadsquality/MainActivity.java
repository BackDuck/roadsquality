package com.example.nurshat.roadsquality;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;
import java.util.Calendar;
import android.provider.Settings.Secure;

public class MainActivity extends AppCompatActivity {
    TextView tvX, tvY, tvZ, tvSum, tvMaxSum, tvMaxSum2;
    float maxSum = 0;
    private LocationManager locationManager;
    SharedPreferences sPref;
    final String DEVICE_NAME = "device_name";
    final String DEVICE_ID = "device_id";

    long time0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvX = (TextView) findViewById(R.id.x);
        tvY = (TextView) findViewById(R.id.у);
        tvZ = (TextView) findViewById(R.id.z);
        tvSum = (TextView) findViewById(R.id.sum);
        tvMaxSum = (TextView) findViewById(R.id.max_sum);
        tvMaxSum2 = (TextView) findViewById(R.id.max_sum2);
        Calendar c = Calendar.getInstance();
        time0 = c.getTimeInMillis();

        SensorManager sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        int sensorType = Sensor.TYPE_ACCELEROMETER;

        sm.registerListener(mySensorEventListener,
                sm.getDefaultSensor(sensorType),
                SensorManager.SENSOR_DELAY_NORMAL);

        sPref = getSharedPreferences("SharedPrefs", MODE_PRIVATE);

        if (sPref.getString(DEVICE_ID, null)== null & sPref.getString(DEVICE_NAME, null)== null){
            SharedPreferences.Editor ed = sPref.edit();
            ed.putString(DEVICE_NAME, android.os.Build.MODEL);
            ed.putString(DEVICE_ID, Secure.getString(getApplicationContext().getContentResolver(),
                    Secure.ANDROID_ID));
            ed.commit();
            new Thread(new Runnable() {
                public void run() {
                    Server example = new Server();
                    String json = example.deviceJson(Build.MODEL,Secure.getString(getApplicationContext().getContentResolver(),
                            Secure.ANDROID_ID) );

                    try {
                        example.deviceReg("http://test.itsfeature.ru/savedevice.php", json);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }).start();
        }
        Intent serviceIntent = new Intent(this, BackgroundService.class);
        startService(serviceIntent);

    }

    final SensorEventListener mySensorEventListener = new SensorEventListener() {
        public void onSensorChanged(SensorEvent sensorEvent) {
            if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                float x = sensorEvent.values[0];
                float y = sensorEvent.values[1];
                float z = sensorEvent.values[2];

                float accelationSquareRoot = (x * x + y * y + z * z)
                        / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);

                tvX.setText(String.valueOf(x));
                tvY.setText(String.valueOf(y));
                tvZ.setText(String.valueOf(z));
                tvMaxSum2.setText(String.valueOf(accelationSquareRoot));
                float sum = Math.abs(x) + Math.abs(y) + Math.abs(z);
                tvSum.setText(String.valueOf(sum));

                if (accelationSquareRoot > maxSum) {
                    maxSum = accelationSquareRoot;
                    tvMaxSum.setText(String.valueOf(maxSum));
                }

            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    public void onClick(View v) {
        maxSum = 0;
    }

}
