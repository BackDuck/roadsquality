package com.example.nurshat.roadsquality;

import android.Manifest;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class BackgroundService extends IntentService {

    final String LOG_TAG = "myLogs";
    float maxSum = 0;
    private LocationManager locationManager;
    String longtitude;
    String latitude;
    String lastLongtitude = "-1";
    String lastLatitude = "-1";
    String dev_name;
    String dev_id;
    SharedPreferences sPref;
    final String LAST_LONGTITUDE = "last_longtitude";
    final String LAST_LATITUDE = "last_latitude";
    final String DEVICE_NAME = "device_name";
    final String DEVICE_ID = "device_id";

    long time0;
    MediaPlayer mp;
    public BackgroundService() {
        super("myname");
    }

    public void onCreate() {
        super.onCreate();

        Calendar c = Calendar.getInstance();
        time0 = c.getTimeInMillis();

        SensorManager sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        int sensorType = Sensor.TYPE_ACCELEROMETER;

        sm.registerListener(mySensorEventListener,
                sm.getDefaultSensor(sensorType),
                SensorManager.SENSOR_DELAY_NORMAL);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                0, 0, locationListener);
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 0, 0,
                locationListener);
        Log.d(LOG_TAG, "onCreate");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
    }
    final SensorEventListener mySensorEventListener = new SensorEventListener() {
        public void onSensorChanged(SensorEvent sensorEvent) {
            if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                float x = sensorEvent.values[0];
                float y = sensorEvent.values[1];
                float z = sensorEvent.values[2];

                float accelationSquareRoot = (x * x + y * y + z * z)
                        / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
                float sum = Math.abs(x) + Math.abs(y) + Math.abs(z);
                Calendar c = Calendar.getInstance();

                if (accelationSquareRoot > 1.6 & longtitude != null) {
                    try {
                        System.out.println(longtitude + " " + latitude);
                        sPref = getSharedPreferences("SharedPrefs", MODE_PRIVATE);
                        lastLatitude = sPref.getString(LAST_LATITUDE, "");
                        lastLongtitude = sPref.getString(LAST_LONGTITUDE, "");
                        dev_name = sPref.getString(DEVICE_NAME, "");
                        dev_id = sPref.getString(DEVICE_ID, "");
                        if (!longtitude.equals(lastLongtitude) & !latitude.equals(lastLatitude)) {

                            long time1 = c.getTimeInMillis();

                            if (time1 - time0 >= 1000) {
                                System.out.println("Отправил!");
                                setCoords(longtitude, latitude, "" + accelationSquareRoot, dev_name, dev_id);
                                mp = MediaPlayer.create(getApplicationContext(), R.raw.povorot);
                                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
                                        // TODO Auto-generated method stub
                                        mp.reset();
                                        mp.release();
                                        mp=null;
                                    }

                                });
                                mp.start();
                                time0 = time1;
                                sPref = getSharedPreferences("SharedPrefs", MODE_PRIVATE);
                                SharedPreferences.Editor ed = sPref.edit();
                                ed.putString(LAST_LATITUDE, latitude);
                                ed.putString(LAST_LONGTITUDE, longtitude);
                                ed.commit();
                            }

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (accelationSquareRoot > maxSum) {
                    maxSum = accelationSquareRoot;
                }

            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };


    public void setCoords(final String lon, final String len, final String lvl, final String name, final String id) throws IOException {
        new Thread(new Runnable() {
            public void run() {
                Server example = new Server();
                String json = example.bowlingJson(lon, len, lvl, name, id);
                try {
                    example.post("http://test.itsfeature.ru/coords.php", json);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();

    }


    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            showLocation(location);
        }

        @Override
        public void onProviderDisabled(String provider) {

        }

        @Override
        public void onProviderEnabled(String provider) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            showLocation(locationManager.getLastKnownLocation(provider));
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    private void showLocation(Location location) {
        if (location == null)
            return;
        if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
            longtitude = Double.toString(location.getLongitude());
            latitude = Double.toString(location.getLatitude());
        } else if (location.getProvider().equals(
                LocationManager.NETWORK_PROVIDER)) {
            longtitude = Double.toString(location.getLongitude());
            latitude = Double.toString(location.getLatitude());
        }
    }

}