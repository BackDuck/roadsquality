package com.example.nurshat.roadsquality;

/**
 * Created by Nurshat on 18.08.2016.
 */


import android.annotation.TargetApi;
import android.media.MediaPlayer;
import android.os.Build;
import android.util.Log;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Server {
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    MediaPlayer mp;
    OkHttpClient client = new OkHttpClient();

    @TargetApi(Build.VERSION_CODES.KITKAT)
    void post(String url, String json) throws IOException {
        RequestBody formBody = new FormBody.Builder()
                .add("json", json)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        Log.d("Server Answer", response.body().string());
    }
    @TargetApi(Build.VERSION_CODES.KITKAT)
    void deviceReg(String url, String json) throws IOException {
        RequestBody formBody = new FormBody.Builder()
                .add("device", json)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        Log.d("Device", response.body().string());


    }

    String bowlingJson(String len, String lon, String lvl, String name, String id) {
        return "{\"len\":\""+len+"\","
                + "\"lon\":\""+lon+"\","
                + "\"lvl\":\""+lvl+"\","
                + "\"device_name\":\""+name+"\","
                + "\"device_id\":\""+id+"\"}";
    }
    String deviceJson(String name, String id) {
        return "{\"device_name\":\""+name+"\","
                + "\"device_id\":\""+id+"\"}";
    }


}